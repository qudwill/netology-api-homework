const supertest = require('supertest');
const expect = require('chai').expect;

describe('REST API', () => {
	let server;

	const APIVersion = '/api/v1';

	before((done) => {
		require('../part1/index');

		setTimeout(() => {
			server = supertest.agent('http://localhost:3000');

			done();
		}, 1000);
	});

	describe('POST /users', () => {
		it('POST /users должен вернуть объект с ID созданного пользователя', done => {
			server
				.post(`${APIVersion}/users`)
				.send({
					name: 'name',
					score: 1
				})
				.expect(200)
				.end((err, res) => {
					expect(res.body.id).to.be.an('number');
					done();
				});
		});
	
		it('POST /users без данных должен вернуть ошибку 400', done => {
			server
				.post(`${APIVersion}/users`)
				.expect(400)
				.end(() => {
					done();
				});
		});
	
		it('POST /users без отправленного name должен вернуть ошибку 400', done => {
			server
				.post(`${APIVersion}/users`)
				.send({
					score: 1
				})
				.expect(400)
				.end(() => {
					done();
				});
		});
	
		it('POST /users без отправленного score должен вернуть ошибку 400', done => {
			server
				.post(`${APIVersion}/users`)
				.send({
					name: 'name'
				})
				.expect(400)
				.end(() => {
					done();
				});
		});
	});

	describe('DELETE /users/:id', () => {
		it('DELETE /users/:id при отправке несуществующего ID вернет ошибку 400', done => {
			server
				.delete(`${APIVersion}/users/100`)
				.expect(400)
				.end(() => {
					done();
				});
		});

		it('DELETE /users без ID вернет ошибку 404', done => {
			server
				.delete(`${APIVersion}/users`)
				.expect(404)
				.end(() => {
					done();
				});
		});

		it('DELETE /users/:id при отправке существующего ID пользователя вернет статус 200', done => {
			server
				.post(`${APIVersion}/users`)
				.send({
					name: 'name',
					score: 1
				})
				.expect(200)
				.end((err, res) => {
					server
						.delete(`${APIVersion}/users/${res.body.id}`)
						.expect(200)
						.end(() => {
							done();
						});
				});
		});
	});	
});