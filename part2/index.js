'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());

const restAPI = express.Router();
const users = [];

/**
 ** Helper Functions
 */

const incorrectName = callback => {
	callback(400, {
		'code': -1,
		'message': 'Bad request. Please provide correct username, params should be object with 2 keys and 2 values: "{name: value, score: value}"'
	});
};

const incorrectScore = callback => {
	callback(400, {
		'code': -2,
		'message': 'Bad request. Please provide correct score value. params should be object with 2 keys and 2 values: "{name: value, score: value}"'
	});
};

const incorrectUserID = callback => {
	callback(400, {
		'code': -3,
		'message': 'Bad request. Please provide ID of user in params: {id: value}'
	});
}

const userDoesntExist = callback => {
	callback(404, {
		'code': -4,
		'message': 'User doesn\'t exist'
	})
}

const RPC = {

	/**
 	** POST Methods
 	*/

	createUser: function(params, callback) {
		if (params.name && params.score) {
			users.push({
				name: params.name,
				score: params.score
			});
	
			callback(null, {
				result: {
					id: users.length - 1
				}
			});
		} else {
			!params.name ? incorrectName(callback) : '';
			!params.score ? incorrectScore(callback) : '';
		}
	},


	/**
	** GET Methods
	*/

	readUsers: function(params, callback) {
		callback(null, {
			result: users
		});
	},

	readUser: function(params, callback) {
		if (params.id) {
			const user = users[params.id];
	
			if (user) {
				callback(null, {
					result: users
				});
			} else{
				userDoesntExist(callback)
			}
		} else {
			!params.id ? incorrectUserID(callback) : '';
		}
	},


	/**
	 ** PUT Methods
	 */

	updateUser: function(params, callback) {
		if (params.name && params.score && params.id) {
			const user = users[params.id];
	
			if (user) {
				users[params.id] = Object.assign(
					{},
					{
						name: params.name,
						score: params.score
					}
				);
		
				callback(null, {
					result: {
						name: params.name,
						score: params.score
					}
				});
			} else {
				userDoesntExist(callback);
			}
		} else {
			!params.name ? incorrectName(callback) : '';
			!params.score ? incorrectScore(callback) : '';
			!params.id ? incorrectUserID(callback) : '';
		}
	},


	/**
 	 ** DELETE Methods
 	 */

 	deleteUser: function(params, callback) {
		if (params.id) {
			users[params.id] = undefined;
	
			callback();
		} else {
			!params.id ? incorrectUserID(callback) : '';
		}
	}
}


/**
 ** JSON-RPC
 */

app.post('/rpc', function(req, res) {
	const method = RPC[req.body.method];

	method(req.body.params, function(error, result) {
		error ? res.status(error) : '';

		res.json(result);
		res.send();
	});
});

app.use('', restAPI);
app.listen(3000);