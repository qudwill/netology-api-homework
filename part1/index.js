'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());

const restAPI = express.Router();
const users = [];

/**
 ** CREATE
 */

restAPI.post('/users/', function(req, res) {
	if (req.body.name && req.body.score) {
		users.push({
			name: req.body.name,
			score: req.body.score
		});
	
		res.json({
			id: users.length - 1
		});
	} else {
		res.status(400);
		res.send();
	}
});


/**
 ** READ
 */

restAPI.get('/users/', function(req, res) {
	res.json(users);
});

restAPI.get('/users/:id', function(req, res) {
	const user = users[req.params.id];

	if (user) {
		res.json(user);
	} else{
		res.status(404);
		res.send();
	}
});


/**
 ** UPDATE
 */

restAPI.put('/users/:id', function(req, res) {
	const user = users[req.params.id];

	if (user && req.body.name && req.body.score) {
		users[req.params.id] = Object.assign(
			{},
			{
				name: req.body.name,
				score: req.body.score
			}
		);

		res.json(user);
	} else {
		res.status(404);
		res.send();
	}
});


/**
 ** DELETE
 */

restAPI.delete('/users/:id', function(req, res) {
	users[req.params.id] = undefined;

	res.send();
});

app.use('/api/v1', restAPI);
app.listen(3000);